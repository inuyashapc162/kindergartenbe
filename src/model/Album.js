import mongoose, { ObjectId, Schema } from "mongoose";

const Album = mongoose.model(
  "Album",
  new Schema({
    albumID: ObjectId,
    albumName: {
      type: String,
      require: true,
    },
    school: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "School",
    },
  })
);

export default Album;
