import mongoose, { ObjectId, Schema } from "mongoose";

const Class = mongoose.model(
  "Class",
  new Schema({
    classID: ObjectId,
    className: {
      type: String,
      required: true,
    },
    isActive: {
      type: Boolean,
      required: true,
    },
    expiryDate: {
      type: Date,
      required: true,
    },
    schoolID: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "School",
    },
  })
);

export default Class;
