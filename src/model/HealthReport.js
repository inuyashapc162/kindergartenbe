import mongoose, { ObjectId, Schema } from "mongoose";

const HealthReport = mongoose.model(
  "HealthReport",
  new Schema(
    {
      HealthReportID: ObjectId,
      HealthReportName: {
        type: String,
        require: true,
      },
      Class: {
        type: mongoose.Schema.Types.ObjectId,
        ref: "Class",
      },
      HealthReportDetail: [
        {
          type: mongoose.Schema.Types.ObjectId,
          ref: "HealthReportDetail",
        },
      ],
      isStatus: {
        type: Boolean,
        default: true,
        require: false,
      },
    },
    {
      timestamps: true,
    }
  )
);

export default HealthReport;
