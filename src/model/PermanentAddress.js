import mongoose, { ObjectId, Schema } from "mongoose";

const permanentAddress = mongoose.model(
    "PermanentAddress",
    new Schema(
        {
            permanentAddressID: ObjectId,
            province: {
                type: String,
                require: true,
            },
            district: {
                type: String,
                require: true,
            },
            subDistrict: {
                type: String,
            },
            placeOfBirth: {
                type: String,
            }
        },
        { timestamps: true },
    ),
);

export default permanentAddress;
