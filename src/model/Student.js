import mongoose, { ObjectId, Schema } from "mongoose";

const Student = mongoose.model(
  "Student",
  new Schema({
    studentID: ObjectId,
    classID: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "Class",
    },
    permanentAddressID: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "PermanentAddress",
    },
    temporaryAddressID: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "TemporaryAddress",
    },
    studentDetailID: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "StudentDetail",
    },
    fullName: {
      type: String,
      require: true,
    },
    grade: {
      type: String,
    },
    dateOfBirth: {
      type: Date,
    },
    gender: {
      type: Boolean,//true là nam, false là nữ
    },
    status: {
      type: String,
    },
    admissionDay: {
      type: Date,
    },
    ethnicGroups: {
      type: String,
    },
    nationality: {
      type: String,
    },
    religion: {
      type: String,
    },
    identifier: {
      type: String,
    },
    issueDate: {
      type: Date,
    },
    issuePlace: {
      type: String,
    },
  })
);

export default Student;
