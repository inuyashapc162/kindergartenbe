import mongoose, { ObjectId, Schema } from "mongoose";

const AttendanceDetail = mongoose.model(
  "AttendanceDetail",
  new Schema({
    studentID: {
      type: mongoose.Schema.Types.ObjectId,
      ref: 'Student',
      required: true
    },
    status: {
      type: Boolean,
      default: false
    }
  },
    { timestamps: true }
  )
);

export default AttendanceDetail;
