import mongoose, { ObjectId, Schema } from "mongoose";

const HealthReportDetail = mongoose.model(
  "HealthReportDetail",
  new Schema(
    {
      HealthReportDetailID: ObjectId,
      Student: {
        type: mongoose.Schema.Types.ObjectId,
        ref: "Student",
      },
      weight: {
        type: String,
        require: true,
      },
      height: {
        type: String,
        require: true,
      },
    },
    {
      timestamps: true,
    }
  )
);

export default HealthReportDetail;
