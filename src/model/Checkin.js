import mongoose, { Schema } from "mongoose";

const Checkin = mongoose.model(
  "Checkin",
  new Schema(
    {
      studentId: {
        type: mongoose.Schema.Types.ObjectId,
        ref: "Student",
      },
      classId: {
        type: mongoose.Schema.Types.ObjectId,
        ref: "Class",
      },
      haveMeal: {
        type: Number,
        default: 1,
      },
    },
    { timestamps: true },
  ),
);

export default Checkin;
