import mongoose, { ObjectId, Schema } from "mongoose";

const Image = mongoose.model(
  "Image",
  new Schema({
    ImageID: ObjectId,
    url: {
      type: String,
      require: true,
    },
    album: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "Album",
    },
  })
);

export default Image;
