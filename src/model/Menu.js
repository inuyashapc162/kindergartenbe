import mongoose, { ObjectId, Schema } from "mongoose";

const Menu = mongoose.model(
    "Menu",
    new Schema({
        menuID: ObjectId,
        schoolID: {
            type: mongoose.Schema.Types.ObjectId,
            ref: "School"
        },
        noWeek: {
            type: String,
            require: true
        },
        // startWeek: {
        //     type: Date,
        //     require: true,
        // },
        // endWeek: {
        //     type: Date,
        //     require: true,
        // },
        grade: {
            type: String,
            require: true,
        },
        fileMenu: {
            type: mongoose.Schema.Types.ObjectId,
            ref: "File",
        },
    })
);

export default Menu;
