import mongoose, { ObjectId, Schema } from "mongoose";

const Role = mongoose.model(
  "Role",
  new Schema({
    id: ObjectId,
    roleName: {
      type: String,
      require: true,
    },
  })
);

export default Role;
