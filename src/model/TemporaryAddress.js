import mongoose, { ObjectId, Schema } from "mongoose";

const temporaryAddress = mongoose.model(
    "TemporaryAddress",
    new Schema(
        {
            temporaryAddressID: ObjectId,
            province: {
                type: String,
                require: true,
            },
            district: {
                type: String,
                require: true,
            },
            subDistrict: {
                type: String,
            },
            village: {
                type: String,
            }
        },
        { timestamps: true },
    ),
);

export default temporaryAddress;
