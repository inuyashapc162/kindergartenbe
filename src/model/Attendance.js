import mongoose, { ObjectId, Schema } from "mongoose";

const Attendance = mongoose.model(
  "Attendance",
  new Schema(
    {
      AttendanceDetail: [{
        type: mongoose.Schema.Types.ObjectId,
        ref: 'AttendanceDetail',
        required: true
      }],
      classID: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Class',
        required: true
      }
    }
  ,
    { timestamps: true }
  )
);

export default Attendance;
