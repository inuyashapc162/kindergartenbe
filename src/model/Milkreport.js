import mongoose, { Schema } from "mongoose";

const Milkreport = mongoose.model(
  "Milkreport",
  new Schema(
    {
      studentId: {
        type: mongoose.Schema.Types.ObjectId,
        ref: "Student",
      },
      classId: {
        type: mongoose.Schema.Types.ObjectId,
        ref: "Class",
      },
      haveMilk: {
        type: Number,
        default: 1,
      },
    },
    { timestamps: true },
  ),
);

export default Milkreport;
