import mongoose, { ObjectId, Schema } from "mongoose";

const studentDetail = mongoose.model(
    "StudentDetail",
    new Schema(
        {
            studentDetailID: ObjectId,
            nickName: {
                type: String,
            },
            isTwoSession: {
                type: Boolean
            },
            isBeneficiaryOfSocialWelfare: {
                type: Boolean
            },
            isCanSwim: {
                type: Boolean
            },
            isDayBoarding: {
                type: Boolean
            },
            ethnicMinorityFather: {
                type: Boolean
            },
            ethnicMinorityMother: {
                type: Boolean
            },
            isDisabled: {
                type: String,
            },

        },
        { timestamps: true },
    ),
);

export default studentDetail;
