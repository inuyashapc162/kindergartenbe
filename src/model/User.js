import mongoose, { ObjectId, Schema } from "mongoose";

const User = mongoose.model(
  "User",
  new Schema(
    {
      userID: ObjectId,
      firstName: {
        type: String,
        require: true,
      },
      lastName: {
        type: String,
        require: true,
      },
      address: {
        type: String,
      },
      phoneNumber: {
        type: String,
      },
      email: {
        type: String,
      },
      avatar: {
        type: mongoose.Schema.Types.ObjectId,
        ref: "Image",
      },
      schoolID: {
        type: mongoose.Schema.Types.ObjectId,
        ref: "School",
      },
      classID: {
        type: mongoose.Schema.Types.ObjectId,
        ref: "Class",
      },
      role: {
        type: mongoose.Schema.Types.ObjectId,
        ref: "Role",
      },
    },
    { timestamps: true }
  )
);

export default User;
