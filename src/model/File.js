import mongoose, { ObjectId, Schema } from "mongoose";

const File = mongoose.model(
  "File",
  new Schema(
    {
      fileId: ObjectId,
      name: String,
      data: Buffer,
      contentType: String,
      type: String,
      user: {
        type: mongoose.Schema.Types.ObjectId,
        ref: "User",
      },
    },
    { timestamps: true }
  )
);

export default File;
