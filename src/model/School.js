import mongoose, { ObjectId, Schema } from "mongoose";

const School = mongoose.model(
  "School",
  new Schema({
    schoolID: ObjectId,
    schoolName: {
      type: String,
      require: true,
    },
    address: {
      type: String,
      require: true,
    },
    phone: {
      type: String,
      require: true,
    },
  }),
);

export default School;
