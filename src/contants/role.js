export const ROLE = Object.freeze({
  ADMIN: "Admin",
  TEACHER: "Teacher",
});
