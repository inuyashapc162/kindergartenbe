import express from "express";
import { roleController } from "../controllers/index.js";

// Khai báo đối tượng router
const roleRouter = express.Router();

roleRouter.get("/", roleController.getAllRole);

export default roleRouter;
