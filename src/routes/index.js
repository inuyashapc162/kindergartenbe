import userRouter from "./user.js";
import schoolRouter from "./school.js";
import accountRouter from "./account.js";
import classRouter from "./class.js";
import teacherRouter from "./teacher.js";
import uploadRouter from "./upload.js";
import imageRouter from "./image.js";
import studentRouter from "./student.js";
import fileRouter from "./file.js";
import mealReportRouter from "./mealReport.js";
import attendanceRouter from "./attendance.js";
import attendanceDetailRouter from "./attendanceDetail.js";
import healthReportDetailRouter from "./healthReportDetail.js";
import milkReportRouter from "./milkReport.js";


export {
  userRouter,
  accountRouter,
  schoolRouter,
  classRouter,
  teacherRouter,
  uploadRouter,
  imageRouter,
  studentRouter,
  fileRouter,
  mealReportRouter,
  milkReportRouter,
  attendanceRouter,
  attendanceDetailRouter,
  healthReportDetailRouter,
};
