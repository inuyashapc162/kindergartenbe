import express from "express";
import { classController } from "../controllers/index.js";
import authJwt from "../middleware/authJwt.js";

// Khai báo đối tượng router
const classRouter = express.Router();

classRouter.get("/", [authJwt.verifyToken], classController.getAllClass);
classRouter.get("/:id", classController.getClass);
classRouter.get(
  "/school/:schoolID",
  [authJwt.verifyToken],
  classController.getAllClassBySchoolID
);
classRouter.post("/", classController.createClass);
classRouter.put("/:id", classController.updateClass);

export default classRouter;
