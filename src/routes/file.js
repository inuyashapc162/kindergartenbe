import express from "express";
import { fileController } from "../controllers/index.js";
import multer from "multer";

// Khai báo đối tượng router
const fileRouter = express.Router();
const upload = multer({ dest: "uploads/" });

fileRouter.get("/", fileController.getAllPlan);
fileRouter.get("/:fileId", fileController.downloadFile);
fileRouter.post("/", upload.single("file"), fileController.uploadFile);
fileRouter.delete("/:id", fileController.deletePlan);

export default fileRouter;
