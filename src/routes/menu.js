import express from "express";
import { menuController } from "../controllers/index.js";
import multer from "multer";

// Khai báo đối tượng router
const menuRouter = express.Router();
const upload = multer({ dest: "uploads/" });
// fileRouter.post("/", upload.single("file"), fileController.uploadFile);
// menuRouter.get("/", menuController.getMenu);
menuRouter.get("/", menuController.getMenuByGradeWeek);

menuRouter.post("/", upload.single("fileMenu"), menuController.uploadMenu);

// fileRouter.delete("/:id", fileController.deletePlan);
menuRouter.get("/:schoolId", menuController.downloadFile);
export default menuRouter;
