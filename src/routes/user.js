import express from "express";
import { userController } from "../controllers/index.js";
import { upload } from "../services/upload.js";

// Khai báo đối tượng router
const userRouter = express.Router();

userRouter.get("/:id", userController.getUserDetail);
userRouter.patch("/:id", upload.single("avatar"), userController.changeProfile);
userRouter.post("/:id", upload.single("avatar"), userController.uploadImage);

export default userRouter;
