import express from "express";
import { imageController } from "../controllers/index.js";
import { upload } from "../services/upload.js";
import authJwt from "../middleware/authJwt.js";

// Khai báo đối tượng router
const imageRouter = express.Router();

imageRouter.get("/", imageController.getAllImage);
imageRouter.post(
  "/",
  [authJwt.verifyToken, authJwt.isAdmin],
  upload.single("avatar"),
  imageController.uploadImage
);

export default imageRouter;
