import express from "express";
import { mealReportController } from "../controllers/index.js";

const mealReportRouter = express.Router();

mealReportRouter.get("/", mealReportController.getMealReport);
mealReportRouter.put("/", mealReportController.updateMealReport);

export default mealReportRouter;
