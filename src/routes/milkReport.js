import express from "express";
import { milkReportController } from "../controllers/index.js";

const milkReportRouter = express.Router();

milkReportRouter.get("/", milkReportController.getMilkReport);
milkReportRouter.put("/", milkReportController.updateMilkReport);

export default milkReportRouter;
