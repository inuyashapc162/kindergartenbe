import express from "express";
import { studentController } from "../controllers/index.js";

// Khai báo đối tượng router
const studentRouter = express.Router();

studentRouter.get("/", studentController.getAllStudent);
studentRouter.get("/:id", studentController.getStudentDetail);
studentRouter.post("/", studentController.createStudent);
studentRouter.patch("/", studentController.updateStudent);

export default studentRouter;
