import express from "express";
import { attendanceDetailController } from "../controllers/index.js";

// Khai báo đối tượng router
const attendanceDetailRouter = express.Router();
attendanceDetailRouter.get("/", attendanceDetailController.getAttendanceReport);
attendanceDetailRouter.get("/getByMonth", attendanceDetailController.getAttendanceByMonth);
attendanceDetailRouter.put("/", attendanceDetailController.markAttendance);
attendanceDetailRouter.put(
  "/initializeAttendanceDetail",
  attendanceDetailController.initializeAttendanceDetails
);

export default attendanceDetailRouter;
