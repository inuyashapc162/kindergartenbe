import express from "express";
import { healthReportDetailController } from "../controllers/index.js";

// Khai báo đối tượng router
const healthReportDetailRouter = express.Router();
healthReportDetailRouter.get("/", healthReportDetailController.getAllData);
healthReportDetailRouter.get(
  "/:id",
  healthReportDetailController.getDataDetail
);
healthReportDetailRouter.delete(
  "/:id",
  healthReportDetailController.deleteHealthReport
);
healthReportDetailRouter.put(
  "/changeHealthReportDetail",
  healthReportDetailController.changeHealthReport
);
healthReportDetailRouter.put(
  "/initializeHealthReportDetail",
  healthReportDetailController.initializeHealthDetail
);
export default healthReportDetailRouter;
