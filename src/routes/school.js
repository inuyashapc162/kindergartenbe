import express from "express";
import { schoolController } from "../controllers/index.js";

// Khai báo đối tượng router
const schoolRouter = express.Router();

schoolRouter
    .get("/", schoolController.getAllSchool)
    .get("/:id", schoolController.getSchoolDetail)
    .post("/", schoolController.createSchool)
    .patch("/:id", schoolController.updateSchool);

export default schoolRouter;