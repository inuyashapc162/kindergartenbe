import express from "express";
import uploadImage from "../controllers/upload.js";
import { upload } from "../services/upload.js";

// Khai báo đối tượng router
const uploadRouter = express.Router();

uploadRouter.post("/", upload.single("avatar"), uploadImage);

export default uploadRouter;
