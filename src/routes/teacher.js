import express from "express";
import { teacherController } from "../controllers/index.js";
import authJwt from "../middleware/authJwt.js";

// Khai báo đối tượng router
const teacherRouter = express.Router();

teacherRouter
  .get("/",  teacherController.getAllTeachers)
  .get("/:id", teacherController.getTeacherDetail);
teacherRouter
  .post("/",[authJwt.verifyToken, authJwt.isPrincipal], teacherController.createTeacher)
  .put("/:id",[authJwt.verifyToken, authJwt.isPrincipal], teacherController.updateTeacherDetail);

export default teacherRouter;
