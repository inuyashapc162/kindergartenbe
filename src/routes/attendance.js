import express from "express";
import { attendanceController } from "../controllers/index.js";

const attendanceRouter = express.Router();

export default attendanceRouter;
