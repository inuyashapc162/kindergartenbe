import fs from "fs";
import { menuRepository } from "../repositories/index.js";
// import { fileRepository } from "../repositories/index.js";
// import { get } from "mongoose";
import { MENU } from "../contants/file.js"

const getMenuByGradeWeek = async (req, res) => {
    const schoolId = req.query.schoolId;
    const grade = req.query.grade;
    const noWeek = req.query.noWeek;
    try {
        const result = await menuRepository.getMenuByGradeWeek(schoolId, grade, noWeek);
        const base64Data = result.length > 0 ? Buffer.from(result[0].fileMenu.data).toString('base64') : "";
        res.status(200).send({ message: "Get data successfully", data: base64Data });
    } catch (error) {
        res.status(500).send(error);
    }
};

const uploadMenu = async (req, res) => {
    const fileMenu = req.file;
    const { schoolID, noWeek, grade, userId } = req.body;
    const fileData = {
        name: fileMenu.originalname,
        data: fs.readFileSync(fileMenu.path),
        contentType: fileMenu.mimetype,
        type: MENU,
        user: userId,
    };
    try {
        const result = await menuRepository.uploadMenu(schoolID, noWeek, grade, fileData);
        res.status(200).send({
            message: "Upload menu success",
            data: result
        });
    } catch (error) {
        res.status(500).send(error);
    }
};

const downloadFile = async (req, res) => {
    try {
        const schoolId = req.params.schoolId;
        const file = await menuRepository.downloadFile(schoolId);
        console.log(file[0].fileMenu?.name);
        if (!file) {
            return res.status(404).send("File not found");
        }
        const fileMenu = file[0].fileMenu;
        const encodedFileName = encodeURIComponent(fileMenu?.name);
        res.set({
            "Content-Type": fileMenu?.contentType,
            "Content-Disposition": `attachment; filename="${encodedFileName}"`,
        });
        res.send(fileMenu?.data);
    } catch (error) {
        console.error(error);
        res.status(500).send("Error downloading file");
    }
};

export default { getMenuByGradeWeek, uploadMenu, downloadFile }