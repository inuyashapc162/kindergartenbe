import {
  attendanceDetailRepository,
  healthReportDetailRepository,
} from "../repositories/index.js";

const changeHealthReport = async (req, res) => {
  const { id, weight, height } = req.body;
  try {
    const changeHealthReport =
      await healthReportDetailRepository.changeHealthReport({
        id,
        weight,
        height,
      });
    return res.status(201).json({
      message: "Change health successfully.",
      data: changeHealthReport,
    });
  } catch (error) {
    return res.status(500).json({ message: error.toString() });
  }
};

const initializeHealthDetail = async (req, res) => {
  const { classID, HealthReportName } = req.body;
  try {
    const result = await healthReportDetailRepository.initializeHealthDetail(
      classID,
      HealthReportName
    );
    return res.status(200).json({
      message: "Attendance successfully.",
      data: result,
    });
  } catch (error) {
    return res.status(500).json({ message: error.toString() });
  }
};

const getAllData = async (req, res) => {
  try {
    const result = await healthReportDetailRepository.getAllData();
    return res.status(200).json({
      message: "Get all data successfully.",
      data: result,
    });
  } catch (error) {
    return res.status(500).json({ message: error.toString() });
  }
};

const getDataDetail = async (req, res) => {
  const { id } = req.params;
  try {
    const result = await healthReportDetailRepository.getDataDetail(id);
    return res.status(200).json({
      message: "Get data successfully.",
      data: result,
    });
  } catch (error) {
    return res.status(500).json({ message: error.toString() });
  }
};

const deleteHealthReport = async (req, res) => {
  const { id } = req.params;
  try {
    const result = await healthReportDetailRepository.deleteHealthReport(id);
    return res.status(200).json({
      message: "Delete successfully.",
      data: result,
    });
  } catch (error) {
    return res.status(500).json({ message: error.toString() });
  }
};
export default {
  getAllData,
  initializeHealthDetail,
  changeHealthReport,
  getDataDetail,
  deleteHealthReport,
};
