import { imageRepository } from "../repositories/index.js";
import { uploadToCloudinary } from "../services/upload.js";
import ErrorHandler from "../utils/errorHandler.js";
import bufferToDataURI from "../utils/file.js";

const uploadImage = async (req, res, next) => {
  try {
    const { file } = req;
    if (!file) throw new ErrorHandler(400, "Image is required");

    const fileFormat = file.mimetype.split("/")[1];
    const { base64 } = await bufferToDataURI(fileFormat, file.buffer);

    const imageDetails = await uploadToCloudinary(base64, fileFormat);
    const result = await imageRepository.uploadImage(imageDetails.url);
    return res.status(200).json({
      message: "Upload successful",
      data: result,
    });
  } catch (error) {
    next(new ErrorHandler(error.statusCode || 500, error.message));
  }
};

const getAllImage = async (req, res) => {
  try {
    const result = await imageRepository.getAllImage();
    return res.status(200).json({
      message: "Get all data successfully.",
      data: result,
    });
  } catch (error) {
    return res.status(500).json({ message: error.toString() });
  }
};
export default { uploadImage, getAllImage };
