import userController from "./user.js";
import schoolController from "./school.js";
import accountController from "./account.js";
import classController from "./class.js";
import teacherController from "./teacher.js";
import imageController from "./images.js";
import studentController from "./student.js";
import fileController from "./file.js";
import mealReportController from "./mealReport.js";
import roleController from "./role.js";
import menuController from "./menu.js";
import attendanceController from "./attendance.js";
import attendanceDetailController from "./attendanceDetail.js";
import healthReportDetailController from "./healthReportDetail.js";
import milkReportController from "./milkReport.js";

export {
  userController,
  accountController,
  schoolController,
  classController,
  teacherController,
  imageController,
  studentController,
  fileController,
  mealReportController,
  roleController,
  menuController,
  attendanceController,
  attendanceDetailController,
  healthReportDetailController,
  milkReportController,
};
