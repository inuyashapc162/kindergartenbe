import fs from "fs";
import { fileRepository } from "../repositories/index.js";
import { EDUCATION } from "../contants/file.js";
const uploadFile = async (req, res) => {
  try {
    const file = req.file;
    const { userId } = req.body;
    console.log("🚀 ========= userId:", userId);
    const fileData = {
      name: file.originalname,
      data: fs.readFileSync(file.path),
      contentType: file.mimetype,
      type: EDUCATION,
      user: userId,
    };
    const savedFile = await fileRepository.uploadFile(fileData);
    console.log("🚀 ========= savedFile:", savedFile);
    res.status(200).send("File uploaded successfully");
  } catch (error) {
    console.error(error);
    res.status(500).send("Error uploading file");
  }
};
const downloadFile = async (req, res) => {
  try {
    const fileId = req.params.fileId;
    const file = await fileRepository.downloadFile(fileId);

    // Kiểm tra xem file có tồn tại không
    if (!file) {
      return res.status(404).send("File not found");
    }

    // Thiết lập header cho phản hồi để tải file
    res.set({
      "Content-Type": file.contentType,
      "Content-Disposition": `attachment; filename="${file.name}"`,
    });

    // Trả dữ liệu file
    res.send(file.data);
  } catch (error) {
    console.error(error);
    res.status(500).send("Error downloading file");
  }
};

const getAllPlan = async (req, res) => {
  try {
    const result = await fileRepository.getAllPlan();
    res.status(200).send({
      message: "Get data successful",
      data: result,
    });
  } catch (error) {
    res.status(500).send(error);
  }
};

const deletePlan = async (req, res) => {
  const { id } = req.params;
  try {
    const result = await fileRepository.deletePlan(id);
    res.status(200).send({
      message: "Delete data successful",
      data: result,
    });
  } catch (error) {
    res.status(500).send({
      message: "Delete data not successful",
      data: error,
    });
  }
};
export default { uploadFile, downloadFile, getAllPlan, deletePlan };
