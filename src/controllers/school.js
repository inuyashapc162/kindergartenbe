import { schoolRepository } from "../repositories/index.js";
const createSchool = async (req, res) => {
    const {
        schoolName,
        address,
        phone
    } = req.body;
    try {
        const newSchool = await schoolRepository.createSchool({
            schoolName,
            address,
            phone
        });
        return res.status(201).json({
            message: "Create successfully.",
            data: newSchool,
        });
    } catch (error) {
        return res.status(500).json({ "error on createSchool repo": error.toString() });
    }
};

const getAllSchool = async (req, res) => {
    console.log("hello");
    try {
        const result = await schoolRepository.getAllSchool();
        return res.status(200).json({
            message: "Get all data successfully.",
            data: result,
        });
    } catch (error) {
        return res.status(500).json({ message: error.toString() });
    }
};


const updateSchool = async (req, res) => {
    const { id } = req.params
    console.log("school", req.body);
    const { schoolName, address, phoneNumber } = req.body
    try {
        const result = await schoolRepository.updateSchool(id, schoolName, address, phoneNumber)
        return res.status(200).json({
            message: "Update successfully.",
        });
    } catch (error) {
        return res.status(500).json({ message: error.toString() });
    }
}

const getSchoolDetail = async (req, res) => {
    const id = req.params.id
    console.log("id", id);
    try {
        const result = await schoolRepository.getSchoolDetail(id)
        return res.status(200).json({
            message: "successfully.",
            data: result
        });
    } catch (error) {
        return res.status(500).json({ "get schooldetail controller error": error.toString() });
    }
}
export default {
    createSchool,
    getAllSchool,
    updateSchool,
    getSchoolDetail
};
