import { mealReportRepository } from "../repositories/index.js";

const getMealReport = async (req, res) => {
  const { date, classId } = req.query;
  try {
    const result = await mealReportRepository.getMealReport(date, classId);
    return res.status(200).json(result);
  } catch (error) {
    res.status(500).json({ message: error.message });
  }
};

const updateMealReport = async (req, res) => {
  const data = req.body;
  try {
    const result = await mealReportRepository.updateMealReport(data);
    return res.status(200).json(result);
  } catch (error) {
    res.status(500).json({ message: error.message });
  }
};

export default { getMealReport, updateMealReport };
