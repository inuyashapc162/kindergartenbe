import { roleRepository } from "../repositories/index.js";

const getAllRole = async (req, res) => {
  try {
    const result = await roleRepository.getAllRole();
    return res.status(200).json({
      message: "Get all role data successfully.",
      data: result,
    });
  } catch (error) {
    return res.status(500).json({ message: error.toString() });
  }
};

export default {
  getAllRole,
};
