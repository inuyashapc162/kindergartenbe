import { studentRepository } from "../repositories/index.js";
const createStudent = async (req, res) => {
    const {
        dataStudent,
        dataStudentDetail,
        dataTemAdd,
        dataPerAdd
    } = req.body;
    try {
        const newStudent = await studentRepository.createStudent({
            dataStudent,
            dataStudentDetail,
            dataTemAdd,
            dataPerAdd
        });
        return res.status(201).json({
            message: "Create successfully.",
            data: newStudent,
        });
    } catch (error) {
        return res.status(500).json({ message: error.toString() });
    }
};

const getAllStudent = async (req, res) => {
    const user = req.query;
    try {
        const result = await studentRepository.getAllStudent(user);
        return res.status(200).json({
            message: "Get all data successfully.",
            data: result,
        });
    } catch (error) {
        return res.status(500).json({ "Error in getallstudent controller": error.toString() });
    }
};
const getStudentDetail = async (req, res) => {
    const user = req.params.id;
    console.log("id", user);
    try {
        const result = await studentRepository.getStudentDetail(user);
        return res.status(200).json({
            message: "Get student detail successfully.",
            data: result,
        });
    } catch (error) {
        return res.status(500).json({ "Error in getstudentdetail controller": error.toString() });
    }
};

const updateStudent = async (req, res) => {
    const {
        studentID,
        permanentAddressID,
        temporaryAddressID,
        studentDetailID,
        dataStudent,
        dataStudentDetail,
        dataTemAdd,
        dataPerAdd
    } = req.body;
    // console.log(studentID,
    //     permanentAddressID,
    //     temporaryAddressID,
    //     studentDetailID,
    //     dataStudent,
    //     dataStudentDetail,
    //     dataTemAdd,
    //     dataPerAdd);
    try {
        const newStudent = await studentRepository.updateStudent({
            studentID,
            permanentAddressID,
            temporaryAddressID,
            studentDetailID,
            dataStudent,
            dataStudentDetail,
            dataTemAdd,
            dataPerAdd
        });
        return res.status(201).json({
            message: "Create successfully.",
            data: newStudent,
        });
    } catch (error) {
        return res.status(500).json({ message: error.toString() });
    }
};


export default {
    createStudent,
    getAllStudent,
    updateStudent,
    getStudentDetail
};
