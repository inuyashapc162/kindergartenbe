import { classRepository } from "../repositories/index.js";
import { validationResult } from "express-validator";

const createClass = async (req, res) => {
  const error = validationResult(req);
  if (!error.isEmpty()) {
    return res.status(400).json({ message: error.array()[0].msg });
  }
  const { className, schoolID } = req.body;
  try {
    const newClass = await classRepository.createClass({
      className,
      schoolID,
    });
    return res.status(201).json({
      message: "Create class successfully.",
      data: newClass,
    });
  } catch (error) {
    return res.status(500).json({ message: error.toString() });
  }
};

const getAllClass = async (req, res) => {

  try {
    const result = await classRepository.getAllClass();
    return res.status(200).json({
      message: "Get all class data successfully.",
      data: result,
    });
  } catch (error) {
    return res.status(500).json({ message: error.toString() });
  }
};

const getAllClassBySchoolID = async (req, res) => {
  try {
    const { schoolID } = req.params; 
    const result = await classRepository.getAllClassBySchoolID(schoolID);
    
    return res.status(200).json({
      message: "Get all class data successfully.",
      data: result,
    });
  } catch (error) {
    return res.status(500).json({ message: error.toString() });
  }
};


const getClass = async (req, res) => {
  const { id } = req.params;
  try {
    const result = await classRepository.getClass(id);
    return res.status(200).json({
      message: "Get class data successfully.",
      data: result,
    });
  } catch (error) {
    return res.status(500).json({ message: error.toString() });
  }
};

const updateClass = async (req, res) => {
  const { id } = req.params;
  const error = validationResult(req);

  if (!error.isEmpty()) {
    return res.status(400).json({ message: error.array()[0].msg });
  }

  const {
    className,
    isActive,
    expiryDate: expiryYear
  } = req.body;

  try {
    const currentClass = await classRepository.getClass(id);

    const currentExpiryDate = new Date(currentClass.expiryDate);

    const expiryYearInt = parseInt(expiryYear);

    currentExpiryDate.setUTCFullYear(expiryYearInt);

    await classRepository.updateClass({
      id,
      className,
      isActive,
      expiryDate: currentExpiryDate.toISOString()
    });

    const updatedClass = await classRepository.getClass(id);

    console.log(updatedClass);

    return res.status(201).json({
      message: "Update successfully.",
      data: updatedClass,
    });
  } catch (error) {
    return res.status(500).json({ message: error.toString() });
  }
};






export default {
  createClass,
  getAllClass,
  getClass,
  updateClass,
  getAllClassBySchoolID
};
