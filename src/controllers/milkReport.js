import { milkReportRepository } from "../repositories/index.js";

const getMilkReport = async (req, res) => {
  const { date, classId } = req.query;
  try {
    const result = await milkReportRepository.getMilkReport(date, classId);
    return res.status(200).json(result);
  } catch (error) {
    res.status(500).json({ message: error.message });
  }
};

const updateMilkReport = async (req, res) => {
  const data = req.body;
  try {
    const result = await milkReportRepository.updateMilkReport(data);
    return res.status(200).json(result);
  } catch (error) {
    res.status(500).json({ message: error.message });
  }
};

export default {getMilkReport,updateMilkReport};
