import { attendanceDetailRepository } from "../repositories/index.js";

const markAttendance = async (req, res) => {
  const { studentID, status, date } = req.body;
  try {
    const markAttendance = await attendanceDetailRepository.markAttendance({
      studentID,
      status,
      date,
    });
    return res.status(201).json({
      message: "Attendance successfully.",
      data: markAttendance,
    });
  } catch (error) {
    return res.status(500).json({ message: error.toString() });
  }
};

const initializeAttendanceDetails = async (req, res) => {
  const { classID, date } = req.body;
  try {
    const result = await attendanceDetailRepository.initializeAttendanceDetails(
      { classID, date }
    );
    return res.status(200).json({
      message: "Attendance successfully.",
      data: result,
    });
  } catch (error) {
    return res.status(500).json({ message: error.toString() });
  }
};

const getAttendanceByMonth = async (req, res) => {
  try {
    const { date } = req.query;
    const attendance = await attendanceDetailRepository.getAttendanceByMonth({date});
    return res.status(200).json({
      message: `Get attendance data of ${date} successfully.`,
      data: attendance,
    })
  } catch (error) {
    return res.status(500).json({ message: error.toString() });
  }
}

const getAttendanceReport = async (req, res) => {
  const { date } = req.query;
  const today = new Date(date); // Lấy ngày hôm nay
  today.setHours(0, 0, 0, 0); // Đặt giờ về 00:00:00
  const tomorrow = new Date(today); // Lấy ngày mai
  tomorrow.setDate(today.getDate() + 1); // Cộng thêm 1 ngày để lấy ngày mai

  try {
    const attendanceToday =
      await attendanceDetailRepository.getAttendanceReport(today);
    return res.status(200).json({
      message: "Attendance successfully.",
      data: attendanceToday,
    });
  } catch (error) {
    console.error("Lỗi khi lấy dữ liệu điểm danh:", error);
  }
};
export default {
  markAttendance,
  initializeAttendanceDetails,
  getAttendanceReport,
  getAttendanceByMonth
};
