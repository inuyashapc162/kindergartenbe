import { DEFAULT_PAGE, DEFAUT_LIMIT } from "../contants/pagination.js";
import { teacherRepository } from "../repositories/index.js";

const getAllTeachers = async (req, res) => {
  try {
    const page = parseInt(req.query.page) || DEFAULT_PAGE;
    const limit = parseInt(req.query.limit) || DEFAUT_LIMIT;
    const lastName = req.query.lastName;
    const phoneNumber = req.query.phoneNumber;
    const sortIndex = req.query.sortIndex;
    const sortLastName = req.query.sortLastName;

    const result = await teacherRepository.getAllTeachers(
      page,
      limit,
      lastName,
      phoneNumber,
      sortIndex,
      sortLastName,
    );
    const totalCount = await teacherRepository.getAllTeachersCount();
    return res.status(200).json({
      message: "Get all teachers successfully.",
      data: result,
      currentPage: page,
      totalPages: Math.ceil(totalCount / limit),
      totalCount,
    });
  } catch (error) {
    return res.status(500).json({ message: error.toString() });
  }
};

const createTeacher = async (req, res) => {
  const {
    firstName,
    lastName,
    address,
    phoneNumber,
    email,
    schoolID,
    classID,
  } = req.body;
  try {
    const newUser = await teacherRepository.createTeacher({
      firstName,
      lastName,
      address,
      phoneNumber,
      email,
      schoolID,
      classID,
    });
    return res.status(201).json({
      message: "Create successfully.",
      data: newUser,
    });
  } catch (error) {
    return res.status(500).json({ message: error.toString() });
  }
};

const updateTeacherDetail = async (req, res) => {
  const {
    firstName,
    lastName,
    address,
    phoneNumber,
    email,
    schoolID,
    classID,
  } = req.body;
  try {
    const { id } = req.params;

    const newUser = await teacherRepository.updateTeacherDetail({
      id,
      firstName,
      lastName,
      address,
      phoneNumber,
      email,
      schoolID,
      classID,
    });
    return res.status(201).json({
      message: "Update successfully.",
      data: newUser,
    });
  } catch (error) {
    return res.status(500).json({ message: error.toString() });
  }
};

const getTeacherDetail = async (req, res) => {
  const { id } = req.params;
  try {
    const result = await teacherRepository.getTeacherDetail(id);
    return res.status(200).json({
      message: "Get profile data successfully.",
      data: result,
    });
  } catch (error) {
    return res.status(500).json({ message: error.toString() });
  }
};

export default {
  getAllTeachers,
  createTeacher,
  updateTeacherDetail,
  getTeacherDetail,
};
