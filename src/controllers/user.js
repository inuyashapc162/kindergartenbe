import { userRepository } from "../repositories/index.js";
import { imageRepository } from "../repositories/index.js";
import { uploadToCloudinary } from "../services/upload.js";
import bufferToDataURI from "../utils/file.js";
import ErrorHandler from "../utils/errorHandler.js";

const getUserDetail = async (req, res) => {
  const { id } = req.params;
  try {
    const result = await userRepository.getUserDetail(id);
    return res.status(200).json({
      message: "Get profile data successfully.",
      data: result,
    });
  } catch (error) {
    return res.status(500).json({ message: error.toString() });
  }
};

const uploadImage = async (req, res, next) => {
  const { id } = req.params;
  const { file } = req;
  if (file) {
    try {
      const fileFormat = file.mimetype.split("/")[1];
      const { base64 } = await bufferToDataURI(fileFormat, file.buffer);

      const imageDetails = await uploadToCloudinary(base64, fileFormat);
      const result = await imageRepository.uploadImage(imageDetails.url);
      const newResult = await userRepository.changeProfile({
        id,
        idImage: result._id,
      });
      return res.status(200).json({
        message: "Upload successful",
        data: newResult,
      });
    } catch (error) {
      next(new ErrorHandler(error.statusCode || 500, error.message));
    }
  } else {
    return res.status(200).json({
      message: "No image",
    });
  }
};

const changeProfile = async (req, res) => {
  const { id } = req.params;
  const data = req.body;
  const { file } = req;
  try {
    if (file) {
      if (!file) throw new ErrorHandler(400, "Image is required");
      const fileFormat = file.mimetype.split("/")[1];
      const { base64 } = await bufferToDataURI(fileFormat, file.buffer);
      const imageDetails = await uploadToCloudinary(base64, fileFormat);
      const result = await userRepository.changeProfile({
        id,
        file: imageDetails.url,
      });
      return res.status(200).json({
        message: "Update profile data successfully1.",
        data: { ...result, avatar: imageDetails._id },
      });
    } else {
      const result = await userRepository.changeProfile({ id, data });
      return res.status(200).json({
        message: "Update profile data successfully2.",
        data: result,
      });
    }
  } catch (error) {
    return res.status(500).json({ message: error.toString() });
  }
};
export default {
  getUserDetail,
  changeProfile,
  uploadImage,
};
