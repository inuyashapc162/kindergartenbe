import File from "../model/File.js";

const uploadFile = async (fileData) => {
  try {
    const savedFile = await File.create(fileData);
    return savedFile;
  } catch (error) {
    throw new Error(error);
  }
};

const downloadFile = async (fileId) => {
  try {
    const file = await File.findById(fileId);
    return file;
  } catch (error) {
    throw new Error(error);
  }
};

const getAllPlan = async () => {
  try {
    const result = await File.find({ type: "Education" }).populate("user");
    return result;
  } catch (error) {
    throw new Error(error);
  }
};

const deletePlan = async (id) => {
  try {
    const result = await File.deleteOne({ _id: id });
    return result;
  } catch (error) {
    throw new Error(error);
  }
};
export default { uploadFile, downloadFile, getAllPlan, deletePlan };
