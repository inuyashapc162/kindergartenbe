import Role from "../model/Role.js";

const getAllRole = async () => {
  try {
    const result = await Role.find();
    return result;
  } catch (error) {
    throw new Error(error);
  }
};

export default { getAllRole };
