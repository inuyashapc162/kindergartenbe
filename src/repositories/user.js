import Image from "../model/Image.js";
import User from "../model/User.js";

const getUserDetail = async (id) => {
  try {
    const result = await User.findOne({ _id: id }).populate("avatar");
    return result;
  } catch (error) {
    throw new Error(error);
  }
};

const changeProfile = async ({ id, data, idImage }) => {
  try {
    if (idImage && data) {
      const result = await User.updateOne(
        { _id: id },
        { ...data, avatar: idImage }
      );
      console.log("🚀 ========= result1:", result);
    } else if (data) {
      const result = await User.updateOne({ _id: id }, data);
      console.log("🚀 ========= data1:", data);
      console.log("🚀 ========= result2:", result);
    } else {
      const result = await User.updateOne({ _id: id }, { avatar: idImage });
    }
  } catch (error) {
    throw new Error(error);
  }
};

const createImage = async (url) => {
  try {
    const newImage = await Image.create({ url });
    return newImage;
  } catch (error) {
    throw new Error(error);
  }
};
export default { getUserDetail, changeProfile, createImage };
