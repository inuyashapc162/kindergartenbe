import Menu from "../model/Menu.js";
import File from "../model/File.js";
import fileRepository from "../repositories/file.js";
const uploadMenu = async (schoolID, noWeek, grade, fileData) => {
    try {
        const deleteMenu = await Menu.findOneAndDelete({
            schoolID: schoolID,
            noWeek: noWeek,
            grade: grade
        });
        console.log("delete", deleteMenu);
        if (deleteMenu) {
            await File.deleteOne({ _id: deleteMenu.fileMenu });
        }
        const savedFile = await File.create(fileData);
        const newMenu = await Menu.create({
            schoolID: schoolID,
            noWeek: noWeek,
            grade: grade,
            fileMenu: savedFile._id
        });
        return newMenu;
    } catch (error) {
        console.log("error", error);
    }
};

const downloadFile = async (schoolId) => {
    try {
        const file = await Menu.find({ schoolID: schoolId, grade: 0, noWeek: 0 }).populate('fileMenu');
        return file;
    } catch (error) {
        throw new Error(error);
    }
};

// const getMenuByID = async (id) => {
//     try {
//         const result = await File.find({ _id: id });
//         return result;
//     } catch (error) {
//         throw new Error(error);
//     }
// };

const getMenuByGradeWeek = async (schoolId, grade, noWeek) => {
    console.log(schoolId, grade, noWeek);
    try {
        const result = await Menu.find({ schoolID: schoolId, grade: grade, noWeek: noWeek }).populate('fileMenu');
        console.log(result);
        return result;
    } catch (error) {
        throw new Error(error);
    }
};

export default { uploadMenu, getMenuByGradeWeek, downloadFile };
