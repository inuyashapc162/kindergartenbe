import Student from '../model/Student.js';
import TemAddress from '../model/TemporaryAddress.js';
import PerAddress from '../model/PermanentAddress.js';
import StudentDetail from '../model/StudentDetail.js';
import Class from '../model/Class.js';
const getAllStudent = async (user) => {
    const classID = user.classID;
    try {
        const classData = await Class.find({ _id: classID });
        const result = await Student.find({ classID: classID })
            .populate('studentDetailID')
            .populate('permanentAddressID')
            .populate('temporaryAddressID')
        return { classData, result }
    } catch (error) {
        throw new Error(error)
    }
}

const getStudentDetail = async (id) => {
    try {
        const result = await Student.find({ _id: id })
            .populate('studentDetailID')
            .populate('permanentAddressID')
            .populate('temporaryAddressID')
        return result
    } catch (error) {
        throw new Error(error)
    }
}


const createStudent = async ({
    dataStudent,
    dataStudentDetail,
    dataTemAdd,
    dataPerAdd
}) => {
    try {

        const newTemAddress = await TemAddress.create(dataTemAdd);
        const newPerADdress = await PerAddress.create(dataPerAdd);
        const newStudentDetail = await StudentDetail.create(dataStudentDetail);
        const newStudent = await Student.create({
            permanentAddressID: newPerADdress._id,
            temporaryAddressID: newTemAddress._id,
            studentDetailID: newStudentDetail._id,
            ...dataStudent
        })
        return newStudent;
    } catch (error) {
        throw new Error(error)
    }
}

const updateStudent = async ({
    studentID,
    permanentAddressID,
    temporaryAddressID,
    studentDetailID,
    dataStudent,
    dataStudentDetail,
    dataTemAdd,
    dataPerAdd }) => {
    console.log(permanentAddressID,
        temporaryAddressID,
        studentDetailID,);
    try {
        const editTemAddress = await TemAddress.findOneAndUpdate({ _id: temporaryAddressID }, { ...dataTemAdd });
        if (permanentAddressID === undefined) {
            const newPerADdress = await PerAddress.create(dataPerAdd);
            dataStudent = { ...dataStudent, permanentAddressID: newPerADdress._id };
        }
        else {
            const editPerADdress = await PerAddress.findOneAndUpdate({ _id: permanentAddressID }, { ...dataPerAdd });
        }
        if (studentDetailID === undefined) {
            const newStudentDetail = await StudentDetail.create(dataStudentDetail);
            dataStudent = { ...dataStudent, studentDetailID: newStudentDetail._id };
        }
        else {
            const editStudentDetail = await StudentDetail.findOneAndUpdate({ _id: studentDetailID }, { ...dataStudentDetail });
        }
        const editStudent = await Student.findOneAndUpdate({ _id: studentID }, { ...dataStudent })
        return editStudent;
    } catch (error) {
        throw new Error(error)
    }
}


export default { createStudent, getAllStudent, updateStudent, getStudentDetail };
