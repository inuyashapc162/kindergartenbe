import Student from "../model/Student.js";
import HealthReport from "../model/HealthReport.js";
import HealthReportDetail from "../model/HealthReportDetails.js";
const changeHealthReport = async ({ id, weight, height }) => {
  try {
    let healthReportDetailRecord = await HealthReportDetail.findOne({
      _id: id,
    });
    console.log(
      "🚀 ========= healthReportDetailRecord:",
      healthReportDetailRecord
    );
    if (healthReportDetailRecord) {
      (healthReportDetailRecord.weight = weight),
        (healthReportDetailRecord.height = height);
    } else {
      healthReportDetailRecord = new HealthReportDetail({
        weight,
        height,
      });
    }

    await healthReportDetailRecord.save();

    return {
      message: "Change health successfully",
      data: healthReportDetailRecord,
    };
  } catch (error) {
    return { success: false, message: "Error marking attendance", error };
  }
};

const getAllData = async () => {
  try {
    const result = await HealthReport.find()
      .populate("HealthReportDetail")
      .populate("Class");
    return result;
  } catch (error) {
    throw new Error(error);
  }
};

const initializeHealthDetail = async (classID, HealthReportName) => {
  try {
    const existingRecord = await HealthReportDetail.findOne({
      Class: classID,
    });
    if (!existingRecord) {
      const students = await Student.find({ classID: classID });
      console.log("🚀 ========= students:", students);
      const existingStudent = await HealthReportDetail.findOne({
        studentID: students,
      });
      if (!existingStudent) {
        const healthReportDetails = students.map((student) => ({
          Student: student._id,
        }));
        const createdHealthReportDetails = await HealthReportDetail.insertMany(
          healthReportDetails
        );
        const healthDetailID = createdHealthReportDetails.map(
          (detail) => detail._id
        );
        const newHealthReport = await HealthReport.create({
          HealthReportDetail: createdHealthReportDetails,
          Class: classID,
          HealthReportName,
        });
        return {
          message: "Success",
          healthDetailID,
        };
      }
    }
  } catch (error) {
    throw new Error(error);
  }
};

const getDataDetail = async (id) => {
  try {
    const result = await HealthReport.findOne({ _id: id })
      .populate({
        path: "HealthReportDetail",
        populate: "Student",
      })
      .populate("Class");
    return result;
  } catch (error) {
    throw new Error(error);
  }
};

const deleteHealthReport = async (id) => {
  try {
    const result = await HealthReport.findByIdAndDelete({ _id: id });
    console.log("🚀 ========= result:", result);
    return result;
  } catch (error) {
    throw new Error(error);
  }
};
export default {
  getAllData,
  initializeHealthDetail,
  changeHealthReport,
  getDataDetail,
  deleteHealthReport,
};
