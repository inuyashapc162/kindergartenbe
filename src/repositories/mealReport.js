import Checkin from "../model/Checkin.js";
import Student from "../model/Student.js";
import mongoose from "mongoose";

const getMealReport = async (date, classId) => {
  const selectedDate = new Date(date); // Ví dụ ngày đã chọn
  const selectedMonth = selectedDate.getMonth() + 1; // Tháng dựa trên số 0, vì vậy việc thêm 1
  const selectedYear = selectedDate.getFullYear();

  // Lấy ngày bắt đầu và kết thúc của ngày đã chọn
  const startOfSelectedDate = new Date(selectedDate);
  startOfSelectedDate.setHours(0, 0, 0, 0); // Đặt thời gian thành đầu ngày
  const endOfSelectedDate = new Date(selectedDate);
  endOfSelectedDate.setHours(23, 59, 59, 999); // Đặt thời gian thành cuối ngày
  const selectedClassId = new mongoose.Types.ObjectId(classId);

  const student = await Student.find({ classID: selectedClassId });

  try {
    const result = await Student.aggregate([
      // Lọc sinh viên dựa trên lớp học được chọn
      {
        $match: {
          classID: selectedClassId,
        },
      },
      // Tra cứu thông tin check-in cho từng sinh viên
      {
        $lookup: {
          from: "checkins",
          localField: "_id",
          foreignField: "studentId",
          as: "checkin_data",
        },
      },
      // Giải nén mảng checkin_data
      {
        $unwind: {
          path: "$checkin_data",
          preserveNullAndEmptyArrays: true,
        },
      },
      // Lọc check-in dựa trên tháng và năm được chọn
      {
        $match: {
          $expr: {
            $and: [
              { $eq: [{ $month: "$checkin_data.createdAt" }, selectedMonth] }, // Match month
              { $eq: [{ $year: "$checkin_data.createdAt" }, selectedYear] }, // Match year
            ],
          },
        },
      },
      // Chọn ra các trường để hiển thị, bao gồm thêm thuộc tính haveMeal cho ngày được chọn
      {
        $project: {
          _id: 1,
          fullName: 1,
          studentId: 1,
          createdAt: "$checkin_data.createdAt", // Extract createdAt from checkin_data
          haveMeal: "$checkin_data.haveMeal",
          checkinId: "$checkin_data._id",
        },
      },
      // Nhóm dữ liệu theo ID của sinh viên để đếm số lần check-in và số lần ăn
      {
        $group: {
          _id: "$_id",
          checkinId: {
            $max: {
              $cond: [
                {
                  $and: [
                    { $gte: ["$createdAt", startOfSelectedDate] }, // Kiểm tra nếu createdAt lớn hơn hoặc bằng startOfSelectedDate
                    { $lte: ["$createdAt", endOfSelectedDate] }, /// Kiểm tra nếu createdAt nhỏ hơn hoặc bằng endOfSelectedDate
                  ],
                }, // Kiểm tra nếu createdAt trùng với selectedDate
                "$checkinId", // Trả về haveMeal nếu createdAt trùng với selectedDate
                null, // Return null otherwise
              ],
            },
          },
          fullName: { $first: "$fullName" },
          numberOfCheckins: { $sum: 1 }, // Đếm số lần check-in
          numberOfMeals: {
            $sum: {
              $cond: [
                { $eq: ["$haveMeal", 1] }, // Kiểm tra nếu haveMeal là true
                1, // Tăng thêm 1 nếu haveMeal là true
                0, // Tăng thêm 0 nếu ngược lại
              ],
            },
          },
          haveMealOfSelectedDate: {
            $max: {
              $cond: [
                {
                  $and: [
                    { $gte: ["$createdAt", startOfSelectedDate] }, // Kiểm tra nếu createdAt lớn hơn hoặc bằng startOfSelectedDate
                    { $lte: ["$createdAt", endOfSelectedDate] }, // Kiểm tra nếu createdAt nhỏ hơn hoặc bằng endOfSelectedDate
                  ],
                }, // Kiểm tra nếu createdAt trùng với selectedDate
                "$haveMeal", // Trả về haveMeal nếu createdAt trùng với selectedDate
                null, // Trả về null nếu ngược lại
              ],
            },
          },
        },
      },
      {
        $sort: {
          fullName: 1,
        },
      },
      {
        $facet: {
          totalNumCheckin: [
            {
              $group: {
                _id: null,
                totalCheckins: { $sum: "$numberOfCheckins" },
              },
            },
          ],
          totalNumMeals: [
            {
              $group: {
                _id: null,
                totalMeals: { $sum: "$numberOfMeals" },
              },
            },
          ],

          // Tổng có ăn trong ngày.
          totalHaveMealOfSelectedDate: [
            {
              $group: {
                _id: null,
                totalHaveMealOfSelectedDate: {
                  $sum: {
                    $cond: [
                      { $eq: ["$haveMealOfSelectedDate", 1] }, // Kiểm tra nếu haveMealOfSelectedDate là true
                      1, // Tăng thêm 1 nếu haveMealOfSelectedDate là true
                      0, // Tăng thêm 0 nếu ngược lại
                    ],
                  },
                },
              },
            },
          ],
          // Tổng kh ăn trong ngày.
          totalNotHaveMealOfSelectedDate: [
            {
              $group: {
                _id: null,
                totalNotHaveMealOfSelectedDate: {
                  $sum: {
                    $cond: [
                      { $eq: ["$haveMealOfSelectedDate", 0] }, // Kiểm tra xem hasMealOfSelectedDate có sai không
                      1, // Increment by 1 if haveMealOfSelectedDate is false
                      0, // Increment by 0 otherwise
                    ],
                  },
                },
              },
            },
          ],
          // Khía cạnh thứ hai để lấy dữ liệu từ đường dẫn tổng hợp
          data: [
            { $sort: { fullName: 1 } }, // Sort the data by student name
          ],
        },
      },
    ]);
    // Trích xuất tổng số lượt đăng ký từ kết quả
    const totalNumCheckin = result[0]?.totalNumCheckin[0]?.totalCheckins || 0;
    const totalNumMeals = result[0]?.totalNumMeals[0]?.totalMeals || 0;
    const totalHaveMealOfSelectedDate =
      result[0]?.totalHaveMealOfSelectedDate[0]?.totalHaveMealOfSelectedDate ||
      0;
    const totalNotHaveMealOfSelectedDate =
      result[0]?.totalNotHaveMealOfSelectedDate[0]
        ?.totalNotHaveMealOfSelectedDate || 0;

    // Extract the data from the result
    const data = result[0]?.data.length
      ? result[0]?.data
      : student.length
        ? student.map((item) => ({
            ...item,
            numberOfCheckins: 0,
            numberOfMeals: 0,
            haveMealOfSelectedDate: null,
          }))
        : [];

    return {
      totalNumCheckin,
      totalNumMeals,
      totalHaveMealOfSelectedDate,
      totalNotHaveMealOfSelectedDate,
      data,
    };
  } catch (error) {
    throw new Error(error);
  }
};



const updateMealReport = async (updateData) => {
  try {
    const bulkOps = [];

    updateData.forEach((item) => {
      const { _id, checkinId, haveMeal, date } = item;
      bulkOps.push({
        updateOne: {
          filter: { _id: checkinId },
          update: { haveMeal },
          upsert: true,
        },
        ...(_id === checkinId && {
          insertOne: {
            document: {
              studentId: checkinId,
              haveMeal,
              createdAt: date,
            },
          },
        }),
      });
    });
    const result = await Checkin.bulkWrite(bulkOps); // bulkWrite cho phép thực hiện nhiều loại thao tác (như insert, update, delete)
    return result;
  } catch (error) {
    throw new Error(error);
  }
};

export default { getMealReport, updateMealReport };
