import Milkreport from "../model/Milkreport.js";
import Student from "../model/Student.js";
import mongoose from "mongoose";

const getMilkReport = async (date, classId) => {
  const selectedDate = new Date(date); // Ví dụ ngày đã chọn
  const selectedMonth = selectedDate.getMonth() + 1; // Tháng dựa trên số 0, vì vậy việc thêm 1
  const selectedYear = selectedDate.getFullYear();

  // Lấy ngày bắt đầu và kết thúc của ngày đã chọn
  const startOfSelectedDate = new Date(selectedDate);
  startOfSelectedDate.setHours(0, 0, 0, 0); // Đặt thời gian thành đầu ngày
  const endOfSelectedDate = new Date(selectedDate);
  endOfSelectedDate.setHours(23, 59, 59, 999); // Đặt thời gian thành cuối ngày
  const selectedClassId = new mongoose.Types.ObjectId(classId);

  const student = await Student.find({ classID: selectedClassId });

  try {
    const result = await Student.aggregate([
      // Lọc sinh viên dựa trên lớp học được chọn
      {
        $match: {
          classID: selectedClassId,
        },
      },
      // Tra cứu thông tin check-in cho từng sinh viên
      {
        $lookup: {
          from: "milkreports",
          localField: "_id",
          foreignField: "studentId",
          as: "milkreport_data",
        },
      },
      // Giải nén mảng milkreport_data
      {
        $unwind: {
          path: "$milkreport_data",
          preserveNullAndEmptyArrays: true,
        },
      },
      // Lọc check-in dựa trên tháng và năm được chọn
      {
        $match: {
          $expr: {
            $and: [
              { $eq: [{ $month: "$milkreport_data.createdAt" }, selectedMonth] }, // Match month
              { $eq: [{ $year: "$milkreport_data.createdAt" }, selectedYear] }, // Match year
            ],
          },
        },
      },
      // Chọn ra các trường để hiển thị, bao gồm thêm thuộc tính haveMilk cho ngày được chọn
      {
        $project: {
          _id: 1,
          fullName: 1,
          studentId: 1,
          createdAt: "$milkreport_data.createdAt", // Extract createdAt from milkreport_data
          haveMilk: "$milkreport_data.haveMilk",
          milkreportId: "$milkreport_data._id",
        },
      },
      // Nhóm dữ liệu theo ID của sinh viên để đếm số lần check-in và số lần ăn
      {
        $group: {
          _id: "$_id",
          milkreportId: {
            $max: {
              $cond: [
                {
                  $and: [
                    { $gte: ["$createdAt", startOfSelectedDate] }, // Kiểm tra nếu createdAt lớn hơn hoặc bằng startOfSelectedDate
                    { $lte: ["$createdAt", endOfSelectedDate] }, /// Kiểm tra nếu createdAt nhỏ hơn hoặc bằng endOfSelectedDate
                  ],
                }, // Kiểm tra nếu createdAt trùng với selectedDate
                "$milkreportId", // Trả về haveMilk nếu createdAt trùng với selectedDate
                null, // Return null otherwise
              ],
            },
          },
          fullName: { $first: "$fullName" },
          numberOfMilkreports: { $sum: 1 }, // Đếm số lần check-in
          numberOfMilks: {
            $sum: {
              $cond: [
                { $eq: ["$haveMilk", 1] }, // Kiểm tra nếu haveMilk là true
                1, // Tăng thêm 1 nếu haveMilk là true
                0, // Tăng thêm 0 nếu ngược lại
              ],
            },
          },
          haveMilkOfSelectedDate: {
            $max: {
              $cond: [
                {
                  $and: [
                    { $gte: ["$createdAt", startOfSelectedDate] }, // Kiểm tra nếu createdAt lớn hơn hoặc bằng startOfSelectedDate
                    { $lte: ["$createdAt", endOfSelectedDate] }, // Kiểm tra nếu createdAt nhỏ hơn hoặc bằng endOfSelectedDate
                  ],
                }, // Kiểm tra nếu createdAt trùng với selectedDate
                "$haveMilk", // Trả về haveMilk nếu createdAt trùng với selectedDate
                null, // Trả về null nếu ngược lại
              ],
            },
          },
        },
      },
      {
        $sort: {
          fullName: 1,
        },
      },
      {
        $facet: {
          totalNumMilkreport: [
            {
              $group: {
                _id: null,
                totalMilkreports: { $sum: "$numberOfMilkreports" },
              },
            },
          ],
          totalNumMilks: [
            {
              $group: {
                _id: null,
                totalMilks: { $sum: "$numberOfMilks" },
              },
            },
          ],

          // Tổng có ăn trong ngày.
          totalHaveMilkOfSelectedDate: [
            {
              $group: {
                _id: null,
                totalHaveMilkOfSelectedDate: {
                  $sum: {
                    $cond: [
                      { $eq: ["$haveMilkOfSelectedDate", 1] }, // Kiểm tra nếu haveMilkOfSelectedDate là true
                      1, // Tăng thêm 1 nếu haveMilkOfSelectedDate là true
                      0, // Tăng thêm 0 nếu ngược lại
                    ],
                  },
                },
              },
            },
          ],
          // Tổng kh ăn trong ngày.
          totalNotHaveMilkOfSelectedDate: [
            {
              $group: {
                _id: null,
                totalNotHaveMilkOfSelectedDate: {
                  $sum: {
                    $cond: [
                      { $eq: ["$haveMilkOfSelectedDate", 0] }, // Kiểm tra xem hasMilkOfSelectedDate có sai không
                      1, // Increment by 1 if haveMilkOfSelectedDate is false
                      0, // Increment by 0 otherwise
                    ],
                  },
                },
              },
            },
          ],
          // Khía cạnh thứ hai để lấy dữ liệu từ đường dẫn tổng hợp
          data: [
            { $sort: { fullName: 1 } }, // Sort the data by student name
          ],
        },
      },
    ]);
    // Trích xuất tổng số lượt đăng ký từ kết quả
    const totalNumMilkreport = result[0]?.totalNumMilkreport[0]?.totalMilkreports || 0;
    const totalNumMilks = result[0]?.totalNumMilks[0]?.totalMilks || 0;
    const totalHaveMilkOfSelectedDate =
      result[0]?.totalHaveMilkOfSelectedDate[0]?.totalHaveMilkOfSelectedDate ||
      0;
    const totalNotHaveMilkOfSelectedDate =
      result[0]?.totalNotHaveMilkOfSelectedDate[0]
        ?.totalNotHaveMilkOfSelectedDate || 0;

    // Extract the data from the result
    const data = result[0]?.data.length
      ? result[0]?.data
      : student.length
        ? student.map((item) => ({
            ...item,
            numberOfMilkreports: 0,
            numberOfMilks: 0,
            haveMilkOfSelectedDate: null,
          }))
        : [];

    return {
      totalNumMilkreport,
      totalNumMilks,
      totalHaveMilkOfSelectedDate,
      totalNotHaveMilkOfSelectedDate,
      data,
    };
  } catch (error) {
    throw new Error(error);
  }
};



const updateMilkReport = async (updateData) => {
  try {
    const bulkOps = [];

    updateData.forEach((item) => {
      const { _id, milkreportId, haveMilk, date } = item;
      bulkOps.push({
        updateOne: {
          filter: { _id: milkreportId },
          update: { haveMilk },
          upsert: true,
        },
        ...(_id === milkreportId && {
          insertOne: {
            document: {
              studentId: milkreportId,
              haveMilk,
              createdAt: date,
            },
          },
        }),
      });
    });
    const result = await Milkreport.bulkWrite(bulkOps); // bulkWrite cho phép thực hiện nhiều loại thao tác (như insert, update, delete)
    return result;
  } catch (error) {
    throw new Error(error);
  }
};

export default { getMilkReport, updateMilkReport };
