import School from "../model/School.js";

const getAllSchool = async () => {
    try {
        const result = await School.find();
        return result;
    } catch (error) {
        throw new Error(error);
    }
};

const createSchool = async ({
    schoolName,
    address,
    phone,
}) => {
    // console.log("🚀 ========= firstName:", firstName);
    try {
        const newSchool = await School.create({
            schoolName,
            address,
            phone
        });
        console.log(newSchool._id);
        return newSchool;
    } catch (error) {
        throw new Error(error);
    }
};

const updateSchool = async (id, schoolName, address, phoneNumber) => {
    console.log(schoolName, address, phoneNumber);
    try {
        const upSchool = await School.updateOne({ _id: id }, { schoolName, address, phone: phoneNumber })
        return upSchool;
    } catch (error) {
        throw new Error(error);
    }
}

const getSchoolDetail = async (id) => {
    try {
        const schoolDetail = await School.findOne({ _id: id });
        return schoolDetail;
    } catch (error) {
        throw new Error(error);
    }
}

export default { createSchool, getAllSchool, updateSchool, getSchoolDetail };
