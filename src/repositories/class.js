import Class from "../model/Class.js";
import cron from "node-cron";

const getAllClass = async () => {
  try {
    const result = await Class.find();
    console.log("🚀 ========= result:", result);
    return result;
  } catch (error) {
    throw new Error(error);
  }
};

const getAllClassBySchoolID = async (schoolID) => {
  try {
    const result = await Class.find({ schoolID: schoolID });
    console.log("🚀 ========= result:", result);
    return result;
  } catch (error) {
    throw new Error(error);
  }
};

const getClass = async (id) => {
  console.log('aaaaa', id);
  try {
    const result = await Class.findById({ _id: id });
    return result;
  } catch (error) {
    throw new Error(error);
  }
};

const createClass = async ({ className, schoolID }) => {
  try {
    const currentUTCYear = new Date().getUTCFullYear();
    const nextUTCYear = currentUTCYear + 1;
    const result = await Class.create({
      className,
      isActive: true,
      expiryDate: new Date(Date.UTC(nextUTCYear, 11, 30)),
      schoolID
    });
    return result;
  } catch (error) {
    return error.toString();
  }
};

const updateClass = async ({
  id,
  className,
  isActive,
  expiryDate
}) => {
  // console.log("🚀 ========= className:", className);
  // console.log("🚀 ========= isActive:", isActive);
  // console.log("🚀 ========= expiryDate:", expiryDate);
  // console.log("🚀 ========= id:", id);
  try {
    const classInfo = await Class.findByIdAndUpdate(
      { _id: id },
      {
        className,
        isActive,
        expiryDate,
      },
    );

    if (!classInfo) {
      throw new Error("Class not found");
    }

    return classInfo;
  } catch (error) {
    throw new Error(error);
  }
};

//Hàm này tự động chạy hàng ngày, kp export
const updateClassExpired = async () => {
  try {
    const currentDateTime = new Date();
    await Class.updateMany(
      { expiryDate: { $lte: currentDateTime }, isActive: true },
      { $set: { isActive: false } }
    );
    console.log("Class statuses updated successfully");
  } catch (error) {
    console.error("Error updating class statuses:", error.message);
  }
};

cron.schedule("0 0 * * *", updateClassExpired);

export default { getAllClass,getAllClassBySchoolID, getClass, createClass, updateClass };
