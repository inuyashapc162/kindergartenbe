import Image from "../model/Image.js";

const getAllImage = async () => {
  try {
    const result = await Image.find();
    return result;
  } catch (error) {
    throw new Error(error);
  }
};

const uploadImage = async (url) => {
  try {
    const result = await Image.create({ url });
    return result;
  } catch (error) {
    throw new Error(error);
  }
};
export default { getAllImage, uploadImage };
