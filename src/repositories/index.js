import userRepository from "./user.js";
import schoolRepository from "./school.js";
import accountRepository from "./account.js";
import classRepository from "./class.js";
import teacherRepository from "./teacher.js";
import imageRepository from "./image.js";
import studentRepository from "./student.js";
import fileRepository from "./file.js";
import mealReportRepository from "./mealReport.js";
import roleRepository from "./role.js";
import menuRepository from "./menu.js";
import attendanceRepository from "./attendance.js";
import attendanceDetailRepository from "./attendanceDetail.js";
import healthReportDetailRepository from "./healthReportDetail.js";
import milkReportRepository from "./milkReport.js";

export {
  userRepository,
  accountRepository,
  schoolRepository,
  classRepository,
  teacherRepository,
  imageRepository,
  studentRepository,
  fileRepository,
  mealReportRepository,
  roleRepository,
  menuRepository,
  attendanceRepository,
  attendanceDetailRepository,
  healthReportDetailRepository,
  milkReportRepository,

};
