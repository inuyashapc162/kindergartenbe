import Student from "../model/Student.js";
import AttendanceDetail from "../model/AttendanceDetail.js";
import Attendance from "../model/Attendance.js";

const markAttendance = async ({ studentID, status, date }) => {
  try {
    let attendanceDetailRecord = await AttendanceDetail.findOne({
      studentID: studentID,
      createdAt: {
        $gte: new Date(date),
        $lt: new Date(date).setDate(new Date(date).getDate() + 1),
      },
    });

    if (attendanceDetailRecord) {
      attendanceDetailRecord.status = status;
    } else {
      attendanceDetailRecord = new AttendanceDetail({
        studentID: studentID,
        status: status,
      });
    }

    await attendanceDetailRecord.save();

    return { success: true, message: "Attendance marked successfully" };
  } catch (error) {
    return { success: false, message: "Error marking attendance", error };
  }
};

const initializeAttendanceDetails = async ({ classID, date }) => {
  try {
    const existingRecord = await AttendanceDetail.findOne({
      classID: classID,
      createdAt: {
        $gte: new Date(date),
        $lt: new Date(date).setDate(new Date(date).getDate() + 1),
      },
    });

    if (!existingRecord) {
      const students = await Student.find({ classID: classID });
      const existingStudent = await AttendanceDetail.findOne({
        studentID: students,
        createdAt: {
          $gte: new Date(date),
          $lt: new Date(date).setDate(new Date(date).getDate() + 1),
        },
      });

      if (!existingStudent) {
        const attendanceDetails = students.map((student) => ({
          studentID: student._id,
          status: false,
        }));
        const createdAttendanceDetails = await AttendanceDetail.insertMany(
          attendanceDetails
        );
        console.log("aaaaaa", createdAttendanceDetails);
        const attendanceDetailID = createdAttendanceDetails.map(
          (detail) => detail._id
        );
        // let attendanceRecord = await Attendance.findOne({
        //   classID: classID,
        //   createdAt: { $gte: new Date(date), $lt: new Date(date).setDate(new Date(date).getDate() + 1) },
        // });

        // if (!attendanceRecord) {
        //   attendanceRecord = new Attendance([{
        //     attendanceDetails: attendanceDetailID,
        //   }]);
        //   await attendanceRecord.save();
        // }
        const newAttendance = await Attendance.create({
          AttendanceDetail: createdAttendanceDetails,
          classID,
        });
        console.log("aaaaaa", newAttendance);
        // const newResult = await Attendance.findByIdAndUpdate({ _id: newResult._id }, createdAttendanceDetails)
        return {
          success: true,
          message: "Attendance marked successfully",
          attendanceDetailID,
        };
        // return { success: true, message: "Attendance details initialized successfully", attendanceDetailID };
      }
    }
    return {
      success: true,
      message: "Attendance details already exist",
      attendanceDetailIDs: [],
    };
  } catch (error) {
    return {
      success: false,
      message: "Error initializing attendance details",
      error,
    };
  }
};

const getAttendanceByMonth = async ({ date }) => {
  try {
    const [year, month, day] = date.split('-').map(Number);
    const startOfMonth = new Date(year, month - 1, 1);
    const endOfMonth = new Date(year, month, 0);

    const attendanceData = await AttendanceDetail.find({
      createdAt: {
        $gte: startOfMonth,
        $lte: endOfMonth,
      },
    }).populate("studentID", "fullName");

    const attendanceCountByStudent = {};

    attendanceData.forEach(attendance => {
      const studentID = attendance.studentID._id;
      const studentName = attendance.studentID.fullName;
      if (attendance.status === true) {
        if (!attendanceCountByStudent[studentID]) {
          attendanceCountByStudent[studentID] = {
            studentID,
            studentName,
            attendanceCount: 1
          };
        } else {
          attendanceCountByStudent[studentID].attendanceCount++;
        }
      }
    });

    const result = Object.values(attendanceCountByStudent);

    return result;
  } catch (error) {
    console.error(error);
    throw new Error("Error fetching attendance data");
  }
}

const getAttendanceReport = async (date) => {
  try {
    const result = await AttendanceDetail.find({
      createdAt: { $gte: date, $lt: new Date(date.getTime() + 86400000) },
    });
    return result;
  } catch (error) {
    throw new Error(error);
  }
};

export default {
  markAttendance,
  initializeAttendanceDetails,
  getAttendanceReport,
  getAttendanceByMonth
};
