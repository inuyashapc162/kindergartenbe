import User from "../model/User.js";
import Role from "../model/Role.js";
import School from "../model/School.js";
import Class from "../model/Class.js";
import { ROLE } from "../contants/role.js";

const getAllTeachers = async (
  page,
  limit,
  lastName,
  phoneNumber,
  sortIndex,
  sortLastName,
) => {
  try {
    const role = await Role.findOne({ roleName: ROLE.TEACHER });

    if (!role) {
      throw new Error("Role not found");
    }

    const skip = (page - 1) * limit;

    const result = await User.find({
      role: role._id,
      lastName: { $regex: new RegExp(lastName, "i") },
      ...(phoneNumber && { phoneNumber }),
    })
      .skip(skip)
      .limit(limit)
      .sort({
        ...(sortIndex && { createdAt: parseInt(sortIndex) }),
        ...(sortLastName && { lastName: parseInt(sortLastName) }),
      })
      .collation({ locale: "vi", caseLevel: false })
      .populate(["schoolID", "classID"])
      .exec();

    return result;
  } catch (error) {
    throw new Error(error);
  }
};

const getAllTeachersCount = async () => {
  try {
    const role = await Role.findOne({ roleName: ROLE.TEACHER });

    if (!role) {
      throw new Error("Role not found");
    }

    const result = await User.find({ role: role._id }).countDocuments();

    return result;
  } catch (error) {
    throw new Error(error);
  }
};

const createTeacher = async ({
  firstName,
  lastName,
  address,
  phoneNumber,
  email,
  schoolID,
  classID,
}) => {
  try {
    const role = await Role.findOne({ roleName: ROLE.TEACHER });

    if (!role) {
      throw new Error("Role not found");
    }

    if (schoolID) {
      const existingSchools = await School.findById({ _id: schoolID });

      if (!existingSchools) {
        throw new Error("School not found");
      }
    }

    if (classID) {
      const existingClasses = await Class.findById({ _id: classID });

      if (!existingClasses) {
        throw new Error("Class not found");
      }
    }

    const newUser = await User.create({
      firstName,
      lastName,
      address,
      phoneNumber,
      email,
      schoolID,
      classID,
      role: role._id,
    });
    return newUser;
  } catch (error) {
    throw new Error(error);
  }
};

const updateTeacherDetail = async ({
  id,
  firstName,
  lastName,
  address,
  phoneNumber,
  email,
  schoolID,
  classID,
}) => {
  try {
    if (schoolID) {
      const existingSchools = await School.findById({ _id: schoolID });

      if (!existingSchools) {
        throw new Error("School not found");
      }
    }

    if (classID) {
      const existingClasses = await Class.findById({ _id: classID });

      if (!existingClasses) {
        throw new Error("Class not found");
      }
    }
    const updatedUser = await User.findByIdAndUpdate(
      { _id: id },
      {
        firstName,
        lastName,
        address,
        phoneNumber,
        email,
        schoolID,
        classID,
      },
    );

    if (!updatedUser) {
      throw new Error("User not found");
    }

    return updatedUser;
  } catch (error) {
    throw new Error(error);
  }
};

const getTeacherDetail = async (id) => {
  try {
    const result = await User.findOne({ _id: id })
      .populate(["schoolID", "classID"])
      .exec();
    return result;
  } catch (error) {
    throw new Error(error);
  }
};
export default {
  getAllTeachers,
  getAllTeachersCount,
  createTeacher,
  updateTeacherDetail,
  getTeacherDetail,
};
