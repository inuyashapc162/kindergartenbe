import express from "express";
import * as dotenv from "dotenv";
import cors from "cors";
import multer from "multer";
import ConnectDB from "./database/database.js";
import {
  accountRouter,
  userRouter,
  schoolRouter,
  classRouter,
  teacherRouter,
  uploadRouter,
  imageRouter,
  studentRouter,
  fileRouter,
  mealReportRouter,
  milkReportRouter,
  attendanceRouter,
  attendanceDetailRouter,
  healthReportDetailRouter,
} from "./routes/index.js";
import path from "path";
import roleRouter from "./routes/role.js";
import menuRouter from "./routes/menu.js";

const app = express();
app.use(express.json());
dotenv.config();
app.use(cors());

const port = process.env.PORT || 8080;

app.get("/", (req, res) => {
  res.send("Hello World!");
});
const url = process.env.MONGO_URI;
// Create a storage object with a given configuration
// const storage = new GridFsStorage({
//   url,
//   db: (req, file) => {
//     // If it is an image, save to photos bucket
//     if (file.mimetype === "image/jpeg" || file.mimetype === "image/png") {
//       return "photos";
//     } else {
//       // Otherwise, save to default bucket
//       return null; // or the name of your default bucket
//     }
//   },
//   file: (req, file) => {
//     // If it is an image, save to photos bucket
//     if (file.mimetype === "image/jpeg" || file.mimetype === "image/png") {
//       return {
//         bucketName: "photos",
//         filename: `${Date.now()}_${file.originalname}`,
//       };
//     } else {
//       // Otherwise, save to default bucket
//       return `${Date.now()}_${file.originalname}`;
//     }
//   },
// });

// // Set multer storage engine to the newly created object
// const upload = multer({ storage });

// app.post("/upload/image", upload.single("avatar"), (req, res) => {
//   const file = req.file;
//   // Respond with the file details
//   res.send({
//     message: "Uploaded",
//     id: file.id,
//     name: file.filename,
//     contentType: file.contentType,
//   });
// });
const storage = multer.diskStorage({
  destination: (req, file, cb) => {
    cb(null, "Image");
  },
  filename: (req, file, cb) => {
    console.log(file);
    cb(null, Date.now() + path.extname(file.originalname));
  },
});

// const upload = multer({ storage: storage });
// app.post("/upload", upload.single("avatar"), (req, res) => {
//   res.send("Image Uploaded");
// });
app.use("/upload", uploadRouter);
app.use("/accounts", accountRouter);
app.use("/users", userRouter);
app.use("/schools", schoolRouter);
app.use("/classes", classRouter);
app.use("/teachers", teacherRouter);
app.use("/images", imageRouter);
app.use("/students", studentRouter);
app.use("/files", fileRouter);
app.use("/meal-report", mealReportRouter);
app.use("/roles", roleRouter);
app.use("/menus", menuRouter);
app.use("/attendance", attendanceRouter);
app.use("/attendanceDetail", attendanceDetailRouter);
app.use("/healthcare", healthReportDetailRouter);
app.use("/milk-report", milkReportRouter);



app.listen(port, async () => {
  await ConnectDB();
  console.log(`Example app listening on port ${port}`);
});
