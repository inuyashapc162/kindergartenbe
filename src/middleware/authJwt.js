import jwt from "jsonwebtoken";
import User from "../model/User.js";

const verifyToken = async (req, res, next) => {
  let token = req.headers["token"];
  console.log("🚀 ========= token:", token);
  if (!token) {
    return res.status(403).send({ message: "No token provide!" });
  }

  const jwtSecret = process.env.SECRET_KEY_JWT;
  jwt.verify(token, jwtSecret, (error, decoded) => {
    console.log("🚀 ========= decoded:", decoded);
    if (error) {
      return res.status(401).send({
        message: error,
      });
    }
    // if (!decoded.isActive) {
    //   return res.status(401).send({
    //     message: "User is not active",
    //   });
    // }
    req.userID = decoded.data.user._id;
    console.log("aaaaa", req.userID);
    next();
  });
};

const isAdmin = async (req, res, next) => {
  const { token } = req;
  console.log("🚀 ========= data:", token);
  try {
    const user = await User.findById({ _id: req.userID })
      .populate("role")
      .exec();
    console.log("🚀 ========= user:", user);
    if (!user) {
      return res.status(400).send({ message: "User not found!" });
    }
    if (user.role.roleName === "Admin") {
      next();
    } else {
      return res.status(403).send({ message: "Require Admin Role" });
    }
  } catch (error) {
    return res.status(500).send({ message: error.message });
  }
};

const isPrincipal = async (req, res, next) => {
  const { token } = req;
  console.log("🚀 ========= data:", token);
  try {
    const user = await User.findById({ _id: req.userID })
      .populate("role")
      .exec();
    console.log("🚀 ========= user:", user);
    if (!user) {
      return res.status(400).send({ message: "User not found!" });
    }
    if (user.role.roleName === "Principal") {
      next();
    } else {
      return res.status(403).send({ message: "Require Principal Role" });
    }
  } catch (error) {
    return res.status(500).send({ message: error.message });
  }
};

const isTeacher = async (req, res, next) => {
  const { token } = req;
  console.log("🚀 ========= data:", token);
  try {
    const user = await User.findById({ _id: req.userID })
      .populate("role")
      .exec();
    console.log("🚀 ========= user:", user);
    if (!user) {
      return res.status(400).send({ message: "User not found!" });
    }
    if (user.role.roleName === "Teacher") {
      next();
    } else {
      return res.status(403).send({ message: "Require Teacher Role" });
    }
  } catch (error) {
    return res.status(500).send({ message: error.message });
  }
};

const isParent = async (req, res, next) => {
  const { token } = req;
  console.log("🚀 ========= data:", token);
  try {
    const user = await User.findById({ _id: req.userID })
      .populate("role")
      .exec();
    console.log("🚀 ========= user:", user);
    if (!user) {
      return res.status(400).send({ message: "User not found!" });
    }
    if (user.role.roleName === "Parent") {
      next();
    } else {
      return res.status(403).send({ message: "Require Parent Role" });
    }
  } catch (error) {
    return res.status(500).send({ message: error.message });
  }
};

export default {
  verifyToken,
  isAdmin,
  isPrincipal,
  isTeacher,
  isParent,
};